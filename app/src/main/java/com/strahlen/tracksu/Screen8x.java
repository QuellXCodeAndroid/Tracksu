package com.strahlen.tracksu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Screen8x extends AppCompatActivity {
    Context c;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen8x);
        c = this;
        progressDialog = new ProgressDialog(c,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
    }
    private class Login extends AsyncTask<String, Void, String> {

        String responseStr;

        @Override
        protected String doInBackground(String... arg0) {
            try {
                String registerURL = "http://tracksu-testing.herokuapp.com/api/v1/products";
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(registerURL);
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.addHeader("Access_Token", "e89d681a9bbf8a353d5013209bbffcfc1658");
                HttpResponse response = null;
                HttpEntity resEntity = null;
                response = httpClient.execute(httpPost);
                resEntity = response.getEntity();
                if (resEntity != null) {
                    responseStr = "Dummy";

                    responseStr = EntityUtils.toString(resEntity).trim();
                    //Log.d("Res", responseStr);
                    // you can add an if statement here and do other actions based on the response
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // TODO Auto-generated method stub
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d("Response is :",responseStr);
            JSONObject object = null;
            try {
                if(responseStr.contains("name")){
                    object = new JSONObject(responseStr);
                    progressDialog.dismiss();
                    Toast.makeText(c, "Login Successful.", Toast.LENGTH_SHORT).show();
                    String email_user=object.getString("email");
                    String name_user=object.getString("name");
                    /*editor.putString("name",name_user);
                    editor.putString("email",email_user);
                    editor.commit();
                    Intent n= new Intent(LoginActivity.this,Screen6x2.class);
                    startActivity(n);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();*/
                }else if(responseStr.contains("message")){
                    object = new JSONObject(responseStr);
                    String message=object.getString("message");
                    progressDialog.dismiss();
                    /*Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();*/
                }else{
                    progressDialog.dismiss();
                    /*Toast.makeText(LoginActivity.this, "Sorry something went wrong. Please try again later.", Toast.LENGTH_SHORT).show();*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

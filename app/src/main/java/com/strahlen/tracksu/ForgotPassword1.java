package com.strahlen.tracksu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class ForgotPassword1 extends AppCompatActivity {
    Button cont;
    EditText mEdittext;
    String sEmail;
    JSONObject object,user;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password1);
        cont= (Button) findViewById(R.id.btn_continuefp1);
        mEdittext = (EditText) findViewById(R.id.input_fp1);
        Toast.makeText(this, "Click Continue to proceed.", Toast.LENGTH_SHORT).show();
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate()) {
                    Toast.makeText(ForgotPassword1.this, "Please Enter valid Email Address.", Toast.LENGTH_SHORT).show();
                }else {
                    progressDialog = new ProgressDialog(ForgotPassword1.this,R.style.AppTheme_Dark_Dialog);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Requesting Reset Code...");
                    progressDialog.show();
                    object = new JSONObject();
                    try {
                        object.put("email",sEmail);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    user = new JSONObject();
                    try {
                        user.put("user",object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    RequestEmail request = new RequestEmail();
                    request.execute();
                   /* Intent m = new Intent(ForgotPassword1.this, ForgotPassword2.class);
                    m.putExtra("email",sEmail);
                    startActivity(m);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();*/
                }
            }
        });
    }
    public boolean validate() {
        boolean valid = true;
        sEmail = mEdittext.getText().toString();
        if (sEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(sEmail).matches()) {
            mEdittext.setError("enter a valid email address.");
            valid = false;
        } else {
            mEdittext.setError(null);
        }
        return valid;
    }
    private class RequestEmail extends AsyncTask<String, Void, String> {

        String responseStr;

        @Override
        protected String doInBackground(String... arg0) {
            try {
                String registerURL = "http://tracksu-testing.herokuapp.com/api/v1/users/reset_password";
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(registerURL);
                //httpPost.setHeader("dataType", "application/json");
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.addHeader("Access_Token", "e89d681a9bbf8a353d5013209bbffcfc1658");
                Log.d("request is :",user.toString());
                httpPost.setEntity(new StringEntity(user.toString()));
                HttpResponse response = null;
                HttpEntity resEntity = null;
                response = httpClient.execute(httpPost);
                resEntity = response.getEntity();
                if (resEntity != null) {
                    responseStr = "Dummy";

                    responseStr = EntityUtils.toString(resEntity).trim();
                    //Log.d("Res", responseStr);
                    // you can add an if statement here and do other actions based on the response
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // TODO Auto-generated method stub
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d("Response is :",responseStr);
            JSONObject object = null;
            try {
                if(responseStr.contains("message")){
                    object = new JSONObject(responseStr);
                    String res1 = object.getString("message");
                    if(res1.compareToIgnoreCase("Password reset instructions has been sent")==0) {
                        Toast.makeText(ForgotPassword1.this, "Password reset instructions has been sent to your Email.", Toast.LENGTH_SHORT).show();
                        Intent n = new Intent(ForgotPassword1.this, ForgotPassword2.class);
                        startActivity(n);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        finish();
                    }else{
                        Toast.makeText(ForgotPassword1.this, res1, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(ForgotPassword1.this, "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.strahlen.tracksu;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Screen6x3 extends AppCompatActivity implements OnMapReadyCallback {
    GoogleMap mGooglemap;
    Button back,forward;
    TextView tareekh;
    LatLng current;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen6x3);
        tareekh = (TextView) findViewById(R.id.date6x3);
        back = (Button) findViewById(R.id.back6x3);
        forward = (Button) findViewById(R.id.forward6x3);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Screen6x3.this,Screen6x2.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Screen6x3.this, "It works.", Toast.LENGTH_SHORT).show();
            }
        });
        tareekh.setText(com.strahlen.tracksu.Handler.date);
        if (googleServicesAvailable()){
            initmap();
        }
    }
    private void initmap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }
    public boolean googleServicesAvailable(){
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if(isAvailable == ConnectionResult.SUCCESS){
            return true;
        }else if(api.isUserResolvableError(isAvailable)){
            Dialog dialog = api.getErrorDialog(this, isAvailable,0);
            dialog.show();
        } else{
            Toast.makeText(this, "Cannot connect to google play Services.", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGooglemap = googleMap;
        gotolocation(com.strahlen.tracksu.Handler.lat,com.strahlen.tracksu.Handler.lng,11);
    }

    private void gotolocation(Double lat, Double lng, float zoom) {
        LatLng mylatlon = new LatLng(lat,lng);
        current = new LatLng(lat,lng);
        CameraUpdate cam = CameraUpdateFactory.newLatLngZoom(mylatlon,zoom);
        mGooglemap.moveCamera(cam);
        mGooglemap.addMarker(new MarkerOptions().position(mylatlon).title("Your Location.").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        for (int i=0; i<com.strahlen.tracksu.Handler.mod.size(); i++){
            LatLng loc = new LatLng(Double.parseDouble(com.strahlen.tracksu.Handler.mod.get(i).lat),Double.parseDouble(com.strahlen.tracksu.Handler.mod.get(i).lng));
            mGooglemap.addMarker(new MarkerOptions().position(loc).title(com.strahlen.tracksu.Handler.mod.get(i).naam));
        }
    }
}

package com.strahlen.tracksu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Screen13x extends AppCompatActivity {
    Button finish11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen13x);
        finish11= (Button) findViewById(R.id.btn_finish13x);
        Toast.makeText(this, "Edit Delete will be implemented when items will be added. Click finish to proceed.", Toast.LENGTH_LONG).show();
        finish11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(getApplicationContext(),Screen6x2.class);
                startActivity(n);
                finish();
            }
        });
    }
}

package com.strahlen.tracksu;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "checkins4.db";
    public static final String TABLE_checkins = "checkins4";
    public static final String COLUMN_ID ="_id";
    public static final String COLUMN_placename = "placename";
    public static final String COLUMN_lat = "lat";
    public static final String COLUMN_lng = "lng";
    public static final String COLUMN_time = "time";
    public static final String COLUMN_date = "date";
    public static final String COLUMN_des = "des";
    public MyDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_checkins + " (_id INTEGER PRIMARY KEY AUTOINCREMENT,placename TEXT,lat TEXT,lng TEXT,time TEXT,date TEXT,des TEXT)";
        db.execSQL(query);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_checkins + ";");
        onCreate(db);
    }

    public boolean addProduct(String name, String des, String lat, String lng, String time, String date){
        //Log.d("abc","adding product");
        String name1,des1,lat1,lng1,tym,date1;
        name1=name;
        des1 = des;
        lat1=lat;
        lng1=lng;
        tym = time;
        date1 = date;
        ContentValues values = new ContentValues();
        values.put(COLUMN_placename, name1);
        values.put(COLUMN_lat,lat1);
        values.put(COLUMN_lng,lng1);
        values.put(COLUMN_time,tym);
        values.put(COLUMN_date,date1);
        values.put(COLUMN_des,des1);
        SQLiteDatabase db = getWritableDatabase();
        long result = db.insert(TABLE_checkins, null, values);
        db.close();
        if (result == -1){
            //Log.d("abc","added");
            return false;
        }else{
            //Log.d("abc","not added");
            return true;
        }
    }


    public void deleteProduct(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from "+ TABLE_checkins);
    }

    public Cursor databaseToString(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_checkins,null);
        return c;
    }

}










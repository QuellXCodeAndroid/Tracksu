package com.strahlen.tracksu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by asim_ on 12/22/2016.
 */

public class TimeLineAdapter1 extends RecyclerView.Adapter<TimeLineViewHolder1> {

    private List<Model1> mFeedList;
    private Context mContext;
    private Orientation mOrientation;

    public TimeLineAdapter1(List<Model1> feedList, Orientation orientation) {
        mFeedList = feedList;
        mOrientation = orientation;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

    @Override
    public TimeLineViewHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();

        View view;
//        Log.d("abc","adapterMain");
        if(mOrientation == Orientation.vertical) {
            view = View.inflate(parent.getContext(), R.layout.item_timeline_horizontal, null);
        } else {
            view = View.inflate(parent.getContext(), R.layout.item_timeline1, null);
        }

        return new TimeLineViewHolder1(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder1 holder, int position) {
        //Log.d("abc","setting");
        holder.name.setText(com.strahlen.tracksu.Handler.mod.get(position).naam);
        holder.name1.setText(com.strahlen.tracksu.Handler.mod.get(position).des);
        holder.time1.setText(com.strahlen.tracksu.Handler.mod.get(position).time);
        holder.mTimelineView.setMarker(mContext.getResources().getDrawable(R.drawable.clinic_marker));
        holder.mTimelineView.setmMarkerWidth(80);
        holder.mTimelineView.setmMarkerHeight(520);
    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? mFeedList.size():0);
    }

}

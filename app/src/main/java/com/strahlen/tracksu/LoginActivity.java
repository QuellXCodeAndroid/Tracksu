package com.strahlen.tracksu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.content.Intent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.List;
import butterknife.ButterKnife;
import butterknife.Bind;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    String email,password;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    Context context;
    ProgressDialog progressDialog;
    String cookieString = null;
    JSONObject object;JSONObject user;
    @Bind(R.id.input_email) EditText _emailText;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.btn_login) Button _loginButton;
    @Bind(R.id.link_signup) TextView _signupLink;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        editor = prefs.edit();
        /*if(!prefs.getString("name","").isEmpty())
        {
            Intent intent =  new Intent(LoginActivity.this,Screen10x.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }*/
        setContentView(R.layout.activity_login);
        context = this;
        ButterKnife.bind(this);
        Toast.makeText(this, "Click Login to proceed.", Toast.LENGTH_SHORT).show();
        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    login();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), ForgotPassword1.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            }
        });
    }





    public void login() throws JSONException {
        Log.d(TAG, "Login");
        if (!validate()) {
            onLoginFailed();
            return;
        }
        //_loginButton.setEnabled(false);
        progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
        object = new JSONObject();
        object.put("email",email);
        object.put("password",password);
        user = new JSONObject();
        user.put("user",object);
        Login login = new Login();
        login.execute();
        // TODO: Implement your own authentication logic here.
       /* new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);*/
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }





    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }





    public void onLoginSuccess() {
        //_loginButton.setEnabled(true);
        Intent n = new Intent(LoginActivity.this,Home.class);
        startActivity(n);
        finish();
    }





    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        _loginButton.setEnabled(true);
    }




    public boolean validate() {
        boolean valid = true;
        email = _emailText.getText().toString();
        password = _passwordText.getText().toString();
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address.");
            valid = false;
        } else {
            _emailText.setError(null);
        }if (password.isEmpty()) {
            _passwordText.setError("password cannot be empty.");
            valid = false;
        } else {
            _passwordText.setError(null);
        }
        return valid;
    }




    private class Login extends AsyncTask<String, Void, String> {
        List<HttpCookie> mCookies;
        String responseStr;
        @Override
        protected String doInBackground(String... arg0) {
            try {
                String registerURL = "http://tracksu-testing.herokuapp.com/api/v1/users/log_in";
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(registerURL);
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.addHeader("Access_Token", "e89d681a9bbf8a353d5013209bbffcfc1658");
                Log.e("request is :",user.toString());
                CookieSyncManager.createInstance(context);
                java.net.CookieManager cookieManager = new java.net.CookieManager();
                cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
                CookieHandler.setDefault(cookieManager);
                httpPost.setEntity(new StringEntity(user.toString()));
                HttpResponse response = null;
                HttpEntity resEntity = null;
                response = httpClient.execute(httpPost);
                List<Cookie> cookies = httpClient.getCookieStore().getCookies();
                CookieSyncManager.getInstance().sync();
                if(cookies != null) {
                    for(Cookie cookie : cookies) {
                        cookieString = /*cookie.getName() + "=" +*/ cookie.getValue(); /*+ "; domain=" + cookie.getDomain();*/
                        CookieManager.getInstance().setCookie(cookie.getDomain(), cookieString);
                    }
                }
                resEntity = response.getEntity();
                if (resEntity != null) {
                    responseStr = "Dummy";
                    responseStr = EntityUtils.toString(resEntity).trim();
                    //Log.d("Res", responseStr);
                    // you can add an if statement here and do other actions based on the response
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            if(cookieString != null){
                editor.putString("cookies",cookieString);
            }
            Log.e("cookies are ", cookieString);
            Log.e("Response is :",responseStr);
            JSONObject object = null;
            try {
                if(responseStr.contains("name")){
                    object = new JSONObject(responseStr);
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Login Successful.", Toast.LENGTH_SHORT).show();
                    String email_user=object.getString("email");
                    String name_user=object.getString("name");
                    editor.putString("name",name_user);
                    editor.putString("email",email_user);
                    editor.commit();
                    Intent n= new Intent(LoginActivity.this,Screen10x.class);
                    startActivity(n);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }else if(responseStr.contains("message")){
                    object = new JSONObject(responseStr);
                    String message=object.getString("message");
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                }else{
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Sorry something went wrong. Please try again later.", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.strahlen.tracksu;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class Screen6x extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private TimeLineAdapter mTimeLineAdapter;
    Context c;
    private List<TimeLineModel> mDataList = new ArrayList<>();
    Orientation mOrientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen6x);
        c=this;
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(getLinearLayoutManager());
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0));
        initView();
    }
    private LinearLayoutManager getLinearLayoutManager() {

        if (mOrientation == Orientation.vertical) {

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            return linearLayoutManager;
        } else {

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            return linearLayoutManager;
        }

    }

    private void initView() {

        for(int i = 0;i <5;i++) {
            TimeLineModel model = new TimeLineModel();
            model.setName("Unknown");
            model.setAge("20min");
            model.setTime("9:10");
            mDataList.add(model);
        }

        mTimeLineAdapter = new TimeLineAdapter(mDataList,mOrientation);
        mRecyclerView.setAdapter(mTimeLineAdapter);
    }

}

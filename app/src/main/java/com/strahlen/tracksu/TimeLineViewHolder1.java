package com.strahlen.tracksu;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by asim_ on 12/22/2016.
 */

public class TimeLineViewHolder1 extends RecyclerView.ViewHolder {
    public TextView name,time1,name1;
    public com.strahlen.tracksu.TimelineView mTimelineView;

    public TimeLineViewHolder1(View itemView, int viewType) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.tx_name11);
        mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker1);
        name1 = (TextView) itemView.findViewById(R.id.tx_name12);
        time1= (TextView) itemView.findViewById(R.id.tx_time1);
        mTimelineView.initLine(viewType);
    }
}
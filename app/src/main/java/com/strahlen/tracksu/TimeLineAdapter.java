package com.strahlen.tracksu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by asim_ on 12/21/2016.
 */

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

    private List<TimeLineModel> mFeedList;
    private Context mContext;
    private Orientation mOrientation;

    public TimeLineAdapter(List<TimeLineModel> feedList, Orientation orientation) {
        mFeedList = feedList;
        mOrientation = orientation;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();

        View view;

        if(mOrientation == Orientation.vertical) {
            view = View.inflate(parent.getContext(), R.layout.item_timeline_horizontal, null);
        } else {
            view = View.inflate(parent.getContext(), R.layout.item_timeline, null);
        }

        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {

        TimeLineModel timeLineModel = mFeedList.get(position);

        holder.name.setText(timeLineModel.getName());
        holder.name1.setText(timeLineModel.getAge());
        holder.time.setText(timeLineModel.getTime());
        holder.mTimelineView.setmMarkerWidth(70);
        holder.mTimelineView.setmMarkerHeight(520);
    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? mFeedList.size():0);
    }

}

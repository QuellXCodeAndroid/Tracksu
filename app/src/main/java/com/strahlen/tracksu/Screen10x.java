package com.strahlen.tracksu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import java.io.IOException;

public class Screen10x extends AppCompatActivity {
    ProgressDialog progressDialog;
    Context c;
    SharedPreferences prefs;
    String cooks;
    ListView products;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen10x);
        c = this;
        prefs = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        cooks = prefs.getString("cookies","");
        Log.e("cookies1 are",cooks);
        products = (ListView) findViewById(R.id.products10xlist);
        progressDialog = new ProgressDialog(c, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
        getProducts gp = new getProducts();
        gp.execute();
    }




    private class getProducts extends AsyncTask<String, Void, String> {
        String responseStr;
        @Override
        protected String doInBackground(String... arg0) {
            try {
                String registerURL = "http://tracksu-testing.herokuapp.com/api/v1/products";
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet request = new HttpGet(registerURL);
                request.setHeader("Content-Type", "application/json");
                request.addHeader("Access_Token", "e89d681a9bbf8a353d5013209bbffcfc1658");
                request.addHeader("_tracksu_session",cooks);
                HttpResponse response = null;
                HttpEntity resEntity = null;
                response = httpClient.execute(request);
                resEntity = response.getEntity();
                if (resEntity != null) {
                    responseStr = "Dummy";
                    responseStr = EntityUtils.toString(resEntity).trim();
                    //Log.d("Res", responseStr);
                    // you can add an if statement here and do other actions based on the response
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            // TODO Auto-generated method stub
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            Log.e("Response2 is :",responseStr);
            progressDialog.dismiss();
            JSONObject object = null;
            /*try {
                if(responseStr.contains("name")){
                    object = new JSONObject(responseStr);
                    progressDialog.dismiss();
                    Toast.makeText(c, "Login Successful.", Toast.LENGTH_SHORT).show();
                    *//*String email_user=object.getString("email");
                    String name_user=object.getString("name");*//*
                    *//*editor.putString("name",name_user);
                    editor.putString("email",email_user);
                    editor.commit();
                    Intent n= new Intent(LoginActivity.this,Screen6x2.class);
                    startActivity(n);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();*//*
                }else if(responseStr.contains("message")){
                    object = new JSONObject(responseStr);
                    String message=object.getString("message");
                    progressDialog.dismiss();
                    *//*Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();*//*
                }else{
                    progressDialog.dismiss();
                    *//*Toast.makeText(LoginActivity.this, "Sorry something went wrong. Please try again later.", Toast.LENGTH_SHORT).show();*//*
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }
    }
}

package com.strahlen.tracksu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Screen7x extends AppCompatActivity {
    Button next;
    CheckBox mCheck1,mCheck2,mCheck3,mCheck4,mCheck5,mCheck6,mCheck7;
    String role,whatHappened;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen7x);
        next= (Button) findViewById(R.id.buttonNext);
        mCheck1 = (CheckBox) findViewById(R.id.radio1);
        mCheck2 = (CheckBox) findViewById(R.id.radio2);
        mCheck3 = (CheckBox) findViewById(R.id.radio3);
        mCheck4 = (CheckBox) findViewById(R.id.radio4);
        mCheck5 = (CheckBox) findViewById(R.id.radio5);
        mCheck6 = (CheckBox) findViewById(R.id.radio6);
        mCheck7 = (CheckBox) findViewById(R.id.radio7);
        role = whatHappened = "";
        mCheck1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mCheck2.setChecked(false);
                    role = mCheck1.getText().toString();
                }
            }
        });
        mCheck2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mCheck1.setChecked(false);
                    role = mCheck2.getText().toString();
                }
            }
        });
        mCheck3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mCheck4.setChecked(false);
                    mCheck5.setChecked(false);
                    mCheck6.setChecked(false);
                    mCheck7.setChecked(false);
                    whatHappened = mCheck3.getText().toString();
                }
            }
        });
        mCheck4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mCheck3.setChecked(false);
                    mCheck5.setChecked(false);
                    mCheck6.setChecked(false);
                    mCheck7.setChecked(false);
                    whatHappened = mCheck4.getText().toString();
                }
            }
        });
        mCheck5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mCheck4.setChecked(false);
                    mCheck3.setChecked(false);
                    mCheck6.setChecked(false);
                    mCheck7.setChecked(false);
                    whatHappened = mCheck5.getText().toString();
                }
            }
        });
        mCheck6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mCheck4.setChecked(false);
                    mCheck5.setChecked(false);
                    mCheck3.setChecked(false);
                    mCheck7.setChecked(false);
                    whatHappened = mCheck6.getText().toString();
                }
            }
        });
        mCheck7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mCheck4.setChecked(false);
                    mCheck5.setChecked(false);
                    mCheck6.setChecked(false);
                    mCheck3.setChecked(false);
                    whatHappened = mCheck7.getText().toString();
                }
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent m = new Intent(Screen7x.this,Screen8x.class);
                startActivity(m);
                finish();
            }
        });
    }
}

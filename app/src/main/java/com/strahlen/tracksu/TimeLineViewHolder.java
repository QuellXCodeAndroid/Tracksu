package com.strahlen.tracksu;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by asim_ on 12/21/2016.
 */

public class TimeLineViewHolder extends RecyclerView.ViewHolder {
    public TextView name,time,name1;
    public com.strahlen.tracksu.TimelineView mTimelineView;

    public TimeLineViewHolder(View itemView, int viewType) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.tx_name);
        mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
        name1 = (TextView) itemView.findViewById(R.id.tx_name1);
        time= (TextView) itemView.findViewById(R.id.tx_time);
        mTimelineView.initLine(viewType);
    }
}

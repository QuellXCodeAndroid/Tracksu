package com.strahlen.tracksu;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Screen6xn extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private TimeLineAdapter1 mTimeLineAdapter1;
    Context c;
    private List<TimeLineModel1> mDataList = new ArrayList<>();
    Orientation mOrientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen6xn);
        c=this;
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView2);
        mRecyclerView.setLayoutManager(getLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0));
        initView();
    }
    private LinearLayoutManager getLinearLayoutManager() {

        if (mOrientation == Orientation.vertical) {

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            return linearLayoutManager;
        } else {

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            return linearLayoutManager;
        }

    }

    private void initView() {

        for(int i = 0;i <5;i++) {
            if(i%2==0) {
                TimeLineModel1 model1 = new TimeLineModel1();
                model1.setName("Clinic Name");
                //model1.setAge("Sample");
                model1.setTime("9:10");
                mDataList.add(model1);
            }else{
                TimeLineModel1 model1 = new TimeLineModel1();
                model1.setName("Pharmacy Name");
               // model1.setAge("Sample");
                model1.setTime("9:10");
                mDataList.add(model1);
            }
        }

        //mTimeLineAdapter1 = new TimeLineAdapter1(mDataList,mOrientation);
        mRecyclerView.setAdapter(mTimeLineAdapter1);
    }
}

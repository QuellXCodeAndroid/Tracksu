package com.strahlen.tracksu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Home2 extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 10000;
    Button check_in,today;
    TextView showdb;
    Context c;
    String final_name,lat1,lng1;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    LocationManager lm;
    ProgressDialog dialog1;
    Location loc;
    AlertDialog alertDialog;
    MyDBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        c = this;
        lm = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        dbHandler = new MyDBHandler(this);
        //dbHandler.deleteProduct();
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(c);
            dialog.setMessage("Turn Location On");
            dialog.setPositiveButton(c.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    c.startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(c.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }
        today= (Button) findViewById(R.id.today_activity2);
        showdb = (TextView) findViewById(R.id.showdb);
        today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*String dbString = dbHandler.databaseToString();

                showdb.setText(dbString);*/
                Cursor c = dbHandler.databaseToString();
                if(c.getCount()==0){
                    Toast.makeText(Home2.this, "No Result Found", Toast.LENGTH_SHORT).show();
                }else{
                    StringBuffer buf = new StringBuffer();
                    while(c.moveToNext()){
                        buf.append("name :" + c.getString(1)+ "\n");
                        buf.append("lat :" + c.getString(2)+ "\n");
                        buf.append("lng :" + c.getString(3)+ "\n");
                        showdb.setText(buf.toString());
                    }
                }
            }
        });
        check_in = (Button) findViewById(R.id.check_in2);
        check_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    if (!gps_enabled && !network_enabled) {
                        // notify user
                        AlertDialog.Builder dialog = new AlertDialog.Builder(c);
                        dialog.setMessage("Turn Location On");
                        dialog.setPositiveButton(c.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                // TODO Auto-generated method stub
                                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                c.startActivity(myIntent);
                                recreate();
                                //get gps
                            }
                        });
                        dialog.setNegativeButton(c.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                // TODO Auto-generated method stub

                            }
                        });
                        dialog.show();
                    } else {
                        dialog1 = new ProgressDialog(c);
                        dialog1.setMessage("Fetching Data");
                        dialog1.show();
                        if (ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 10, listener);
                    }
                }else{
                    AlertDialog.Builder dialog = new AlertDialog.Builder(c);
                    dialog.setMessage("Internet is required for Check In.");
                    dialog.setPositiveButton(c.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                            c.startActivity(myIntent);
                            recreate();
                            //get gps
                        }
                    });
                    dialog.setNegativeButton(c.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
                    dialog.show();
                }
            }
        });
    }
    private LocationListener listener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            // TODO Auto-generated method stub



            if (location == null)
            {return;} else {
                loc = location;}

            if (isConnectingToInternet(getApplicationContext())) {

                loc = location;


            }


           // sharedPreferences = getSharedPreferences("PREFS", Context.MODE_PRIVATE);

            Log.d("Lat1 is ", String.valueOf(loc.getLatitude()));
            Log.d("Long1 is ", String.valueOf(loc.getLongitude()));

            if (location!=null)

            {

                try {
                    getAddress(loc.getLatitude(),loc.getLongitude());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }




        }
        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }
    };
    public void getAddress(final double lat, final double lng) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        addresses = geocoder.getFromLocation(lat, lng, 1);
        String address = addresses.get(0).getAddressLine(0);
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
        String pataNhi = addresses.get(0).getPremises();
        dialog1.dismiss();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_label_editor, null);
        dialogBuilder.setView(dialogView);

        final EditText name11 = (EditText) dialogView.findViewById(R.id.name);
        EditText lattt = (EditText) dialogView.findViewById(R.id.lat);
        lattt.setText(String.valueOf(lat));
        Button submit = (Button) dialogView.findViewById(R.id.btn_sub);
       /* submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final_name = name11.getText().toString();
                lat1= String.valueOf(lat);
                lng1= String.valueOf(lng);
                alertDialog.dismiss();
                Log.d("final_ name is :",final_name);
                Log.d("lat1 is :",lat1);
                Log.d("lng1 :",lng1);
                String time = DateFormat.getDateTimeInstance().format(new Date());
                boolean isInserted = dbHandler.addProduct(final_name,lat1,lng1,time);
                if(isInserted == true){
                    Toast.makeText(c, "Inserted!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(c, "Not Inserted!", Toast.LENGTH_SHORT).show();
                }

            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.show();*/

    }
    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

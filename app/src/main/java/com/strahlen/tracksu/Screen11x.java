package com.strahlen.tracksu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Screen11x extends AppCompatActivity {
    Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen11x);
        Toast.makeText(this, "Click Next for next screen.", Toast.LENGTH_SHORT).show();
        next= (Button) findViewById(R.id.btn_next11x);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n=new Intent(getApplicationContext(),Screen12x.class);
                startActivity(n);
                finish();
            }
        });
    }
}

package com.strahlen.tracksu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.strahlen.tracksu.service.gps.GPSLogger;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Screen6x2 extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private TimeLineAdapter1 mTimeLineAdapter1;
    Button checkin,today_activity;
    ArrayList<Model1> mod = new ArrayList<>();
    Context c;
    Switch mSwitch;
    Orientation mOrientation;
    String final_name,des,lat1,lng1,dat,month,year,monthalpha,time,date;
    boolean gps_enabled = false;
    TextView tareekh;
    boolean network_enabled = false;
    LocationManager lm;
    ProgressDialog dialog1,dialog2;
    Location loc;
    AlertDialog alertDialog;
    MyDBHandler dbHandler;
    int check=0;
    PermissionListener permissionlistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen6x2);
        c=this;
        if(android.os.Build.VERSION.SDK_INT>21){
            permissionlistener = new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    Toast.makeText(c, "Permission Granted", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                    Toast.makeText(c, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                }
            };
            new TedPermission(this)
                    .setPermissionListener(permissionlistener)
                    .setRationaleMessage("Tracksu needs permissions for accessing your location and Internet")
                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setGotoSettingButtonText("setting")
                    .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.INTERNET)
                    .check();
        }

        lm = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy");
        SimpleDateFormat df2 = new SimpleDateFormat("MM");
        SimpleDateFormat df3 = new SimpleDateFormat("dd");
        year = df1.format(new Date());
        month = df2.format(new Date());
        dat = df3.format(new Date());
        monthalpha = check_month(month);
        com.strahlen.tracksu.Handler.date = dat+"th"+" "+monthalpha+" "+year;
        dbHandler = new MyDBHandler(this);
        //dbHandler.deleteProduct();
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(c);
            dialog.setMessage("Turn Location On");
            dialog.setPositiveButton(c.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    c.startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(c.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }/*else{
            c.startService(new Intent(c, GPSLogger.class));
        }*/
        tareekh = (TextView) findViewById(R.id.date6x2);
        mSwitch = (Switch) findViewById(R.id.mySwitch6x2);
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(!gps_enabled && !network_enabled){
                        AlertDialog.Builder dialog = new AlertDialog.Builder(c);
                        dialog.setMessage("Turn Location On");
                        dialog.setPositiveButton(c.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                // TODO Auto-generated method stub
                                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                c.startActivity(myIntent);
                                //get gps
                            }
                        });
                        dialog.setNegativeButton(c.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                // TODO Auto-generated method stub

                            }
                        });
                        dialog.show();
                    }else{
                        c.startService(new Intent(c, GPSLogger.class));
                    }
                }else{
                    c.stopService(new Intent(c,GPSLogger.class));
                }
            }
        });
        tareekh.setText(com.strahlen.tracksu.Handler.date);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView1);
        mRecyclerView.setLayoutManager(getLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0));
        //Toast.makeText(c, "Press Checkin to proceed", Toast.LENGTH_LONG).show();
        checkin= (Button) findViewById(R.id.check_in6x2);
        today_activity= (Button) findViewById(R.id.today_activity6x2);
        today_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(com.strahlen.tracksu.Handler.lat==null || com.strahlen.tracksu.Handler.lng==null){
                    Toast.makeText(c, "Please checkin first to view activity.", Toast.LENGTH_SHORT).show();
                }else {
                    Intent m = new Intent(Screen6x2.this, Screen6x3.class);
                    startActivity(m);
                }
            }
        });
        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    check++;
                    //checkin.setEnabled(false);
                    if (!gps_enabled && !network_enabled) {
                        // notify user
                        AlertDialog.Builder dialog = new AlertDialog.Builder(c);
                        dialog.setMessage("Turn Location On");
                        dialog.setPositiveButton(c.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                // TODO Auto-generated method stub
                                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                c.startActivity(myIntent);
                                recreate();
                                //get gps
                            }
                        });
                        dialog.setNegativeButton(c.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                // TODO Auto-generated method stub

                            }
                        });
                        dialog.show();
                    } else {
                        dialog1 = new ProgressDialog(c);
                        dialog1.setMessage("Fetching Location");
                        dialog1.show();
                        if (ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 10, listener);
                        //recreate();
                    }
                }else{
                    final AlertDialog.Builder dialog = new AlertDialog.Builder(c);
                    dialog.setMessage("Internet is required for Check In.");
                    dialog.setPositiveButton(c.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            Intent myIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                            c.startActivity(myIntent);
                            recreate();
                        }
                    });
                    dialog.show();
                }
            }
        });
        initView();
    }

    private String check_month(String month) {
        String alpha = "";
        switch (month){
            case "01":
                alpha = "January";
                break;
            case "02":
                alpha = "February";
                break;
            case "03":
                alpha = "March";
                break;
            case "04":
                alpha = "April";
                break;
            case "05":
                alpha = "May";
                break;
            case "06":
                alpha = "June";
                break;
            case "07":
                alpha = "July";
                break;
            case "08":
                alpha = "August";
                break;
            case "09":
                alpha = "September";
                break;
            case "10":
                alpha = "October";
                break;
            case "11":
                alpha = "November";
                break;
            case "12":
                alpha = "December";
                break;
        }
        return alpha;
    }

    private LocationListener listener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            // TODO Auto-generated method stub
            if (location == null) {
                return;
            }else {
                loc = location;
            }
            if (isConnectingToInternet(getApplicationContext())) {
                loc = location;
            }
            if (location!=null)
            {
                try {
                   /* Handler.lat = loc.getLatitude();
                    Handler.lng = loc.getLongitude();*/
                    getAddress(loc.getLatitude(),loc.getLongitude());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }
    };
    private LinearLayoutManager getLinearLayoutManager() {
        if (mOrientation == Orientation.vertical) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            return linearLayoutManager;
        } else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            return linearLayoutManager;
        }
    }

    private void initView() {
        ////Log.d("abc","initView");
        mod = new ArrayList<>();
        mRecyclerView.setAdapter(null);
        Cursor c = dbHandler.databaseToString();
       // Log.d("abc","cursorget");
        if(c.getCount()==0){
            Toast.makeText(getApplicationContext(), "No Result Found", Toast.LENGTH_SHORT).show();
        }else{
            while(c.moveToNext()){
                String name,desc,lat,lng,time;
                name = c.getString(1);
                lat = c.getString(2);
                lng = c.getString(3);
                time = c.getString(4);
                desc = c.getString(5);
                if(c.move(1)){
                    com.strahlen.tracksu.Handler.lat = Double.valueOf(lat);
                    com.strahlen.tracksu.Handler.lng = Double.valueOf(lng);
                }
                //Log.d("abc","data added");
                mod.add(new Model1(name,desc,lat,lng,time));
            }
        }
        com.strahlen.tracksu.Handler.mod=mod;
        mTimeLineAdapter1 = new TimeLineAdapter1(com.strahlen.tracksu.Handler.mod,mOrientation);
        mRecyclerView.setAdapter(mTimeLineAdapter1);
        //Log.d("abc","adaptercalled");
        //checkin.setEnabled(true);
    }
    public void getAddress(final double lat, final double lng) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        addresses = geocoder.getFromLocation(lat, lng, 1);
        /*String address = addresses.get(0).getAddressLine(0);
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
        String pataNhi = addresses.get(0).getPremises();*/
        dialog1.dismiss();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_label_editor, null);
        dialogBuilder.setView(dialogView);
        final EditText name11 = (EditText) dialogView.findViewById(R.id.name);
        final EditText lattt = (EditText) dialogView.findViewById(R.id.lat);
        Button submit = (Button) dialogView.findViewById(R.id.btn_sub);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final_name = name11.getText().toString();
                des = lattt.getText().toString();
                lat1= String.valueOf(lat);
                lng1= String.valueOf(lng);
                //Log.d("abc","submit");
                boolean isInserted = false;
                SimpleDateFormat df = new SimpleDateFormat("hh:mm ");
                SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
                time = df.format(new Date());
                date = df1.format(new Date());
                alertDialog.dismiss();
                if(check%2==0) {
                    isInserted = dbHandler.addProduct(final_name, des, lat1, lng1, time, date);
                }else{
                    for(int loop=0;loop<2; loop++){
                     isInserted = dbHandler.addProduct(final_name, des, lat1, lng1, time, date);
                    }
                }
                if(isInserted==true){
                    Toast.makeText(c, "Inserted!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(c, "Not Inserted!", Toast.LENGTH_SHORT).show();
                }
                //Log.d("abc","addProduct called");
                //checkin.setEnabled(true);
                initView();
                //recreate();

            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.show();

    }
    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

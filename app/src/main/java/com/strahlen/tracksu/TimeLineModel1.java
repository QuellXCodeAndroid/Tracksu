package com.strahlen.tracksu;

import java.io.Serializable;

/**
 * Created by asim_ on 12/22/2016.
 */

public class TimeLineModel1 implements Serializable {
    private String name;
    private String lat;
    private String lng;
    private String time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }
    public String getLng() {
        return lng;
    }
    public void setLng(String lng) {
        this.lng = lng;
    }
    public void setLat(String lat) {
        this.lat = lat;
    }
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
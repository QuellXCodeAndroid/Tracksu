package com.strahlen.tracksu;

import java.io.Serializable;

/**
 * Created by asim_ on 12/21/2016.
 */

public class TimeLineModel implements Serializable {
    private String name;
    private String age;
    private String time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

package com.strahlen.tracksu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ForgotPassword2 extends AppCompatActivity {
    Button cont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password2);
        cont= (Button) findViewById(R.id.btn_continuefp2);
        Toast.makeText(this, "Click Continue to proceed.", Toast.LENGTH_SHORT).show();
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent m = new Intent(ForgotPassword2.this,LoginActivity.class);
                startActivity(m);
                finish();
            }
        });
    }
}
